# Shell configuration
- [Alactritty](https://alacritty.org/) as Terminal Emulator
- [Fish](https://fishshell.com/) as Shell
- [Starship](https://starship.rs/) as Prompt
