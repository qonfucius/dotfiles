# Neovim

__Requires VimPlug__

Install modules and theme:
`:PlugInstall`

/!\ Make sure to install all LSP dependencies to make it work

_TODO_
- Fix gitsigns error message outside of git repository
