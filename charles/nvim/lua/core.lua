vim.o.relativenumber = true
vim.o.number = true

vim.o.cursorline = true
vim.o.ttyfast = true

vim.o.tabstop = 2
vim.o.shiftwidth = 2
vim.o.softtabstop = 2
vim.o.expandtab = true

vim.o.autoindent = true

vim.o.showmatch = true
vim.o.hlsearch = true
vim.o.incsearch = true

vim.g.mapleader = ' '
