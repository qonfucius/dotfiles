# Alacritty

## Font config

Required dependencies:
- FiraCode Nerd Font installed
- Noto Color Emoji

In `XDG_CONFIG_HOME/fontconfig/fonts.conf`:
```xml
<?xml version="1.0"?>
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">
<fontconfig>
  <alias>
    <family>monospace</family>
    <prefer>
      <family>FiraCode Nerd Font Mono</family>
      <family>Noto Color Emoji</family>
     </prefer>
  </alias>
</fontconfig>
```
