# Fish shell

## Folder structure
```
.
├── completions
│  ├── fnm.fish
│  ├── kubectx.fish -> /opt/kubectx/completion/kubectx.fish
│  └── kubens.fish -> /opt/kubectx/completion/kubens.fish
├── conf.d
│  └── fnm.fish
├── functions
├── config.fish
└── fish_variables
```

## Completion
- [Fnm](https://github.com/Schniz/fnm)
- [Kubectx/Kubens](https://github.com/ahmetb/kubectx)

## Aliased dependencies
- [Exa](https://the.exa.website/)
- [Ripgrep](https://github.com/BurntSushi/ripgrep)
- [Kubectl](https://github.com/kubernetes/kubectl)
- [Bat](https://github.com/sharkdp/bat)
- [Arp-scan](https://github.com/royhills/arp-scan)
