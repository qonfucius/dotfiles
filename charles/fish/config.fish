set fish_greeting
source $HOME/.profile
set -p PATH $HOME/.cargo/bin

starship init fish | source

alias ls="exa -lTL 1 --color=always --group-directories-first"
alias la="exa -laTL 1 --color=always --group-directories-first"
alias lt="exa -laT --color=always --group-directories-first"
alias grep="rg"
alias kg="kubectl get"
alias kd="kubectl describe"
alias kl="kubectl kubectl logs"
alias cat="bat -p --theme Nord"
alias scan="sudo arp-scan --localnet"
alias fwlog='journalctl -t kernel -fag "UFW BLOCK"'

if status is-interactive
    neofetch
end
