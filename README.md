# Qonfucius Dotfiles

> Aggregation of different configuration files from our team

## Rule

- No scripts, just configuration files like `.bashrc`, `.vimrc`, `alacritty.yml`, etc.
- One folder by person, each of us will manage its own folder
- Each folder could follow any architecture, it is for information, inspirations, not for direct install in `.config`

## Current folders

> **Please fill in on update**

- **[Charles](/charles)**
  - alacritty
  - fish
  - starship prompt
  - neovim
- **[Robin](/robin)**
  - zsh
  - tilix
  - gnome-shell
  - neovim
- **[Oscar](/oscar)**
