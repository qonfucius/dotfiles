syntax on

" Highlights searches
set hls
set is

" Add line numbers
set number
" Relative line numbers
set rnu

" Set up tabs as spaces
set tabstop=4
set shiftwidth=4
set expandtab

" Auto indent on new line
set ai

" Hightlight trailing whitespace
highlight TrailingWhitespace ctermbg=red
:autocmd Syntax * syn match TrailingWhitespace /\s\+$/

call plug#begin('~/.vim/plugged')

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
" Plug 'glacambre/firenvim'

call plug#end()
