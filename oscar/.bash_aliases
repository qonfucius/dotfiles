alias vim="nvim"
alias vi="nvim"
alias v="nvim -R -"
alias view="nvim -R"
# edit the aliases file
alias editalias="vi ~/.bash_aliases"
# edit the bashrc file
alias editbash="vi ~/.bashrc"
# edit the hosts file
alias edithosts="sudo nvim /etc/hosts"
# source the bashrc file
alias reload="source ~/.bashrc"
# copy stdin to x clipboard
alias copy="xclip -sel clip"
# keep only first items of each line
alias first="awk '{print \$1}'"
# convert multiline stdin to single line with spaces as separators
alias oneline="paste -s -d ' '"
# make less very easy to type
alias l="less"
# same for nvim -
# nvim is less performant but allows copying text
alias v="nvim -"
# open gitg from the command line
alias gitg="sh -c 'gitg 2>/dev/null 1>/dev/null &'"
# enable/disable touchpad
alias enabletouchpad="gsettings set org.gnome.desktop.peripherals.touchpad send-events 'enabled'"
alias disabletouchpad="gsettings set org.gnome.desktop.peripherals.touchpad send-events 'disabled'"
# base64 decode kubectl json secrets
alias decodesecret="dasel -r yaml -w json | jq '.+{data: .data | map_values(@base64d) }' | dasel -r json -w yaml"
alias decodesecrets="dasel -r yaml -w json | jq '.items | .[]' | jq '.+{data: .data | map_values(@base64d) }' | dasel -r json -w yaml"
# taskwarrior show tasks completed during the past week
alias pastweek="task end.after:today-1wk completed"
# refresh minikube instance, because I keep forgetting the ingress addon
# alias newminikube="minikube delete ; minikube start && minikube addons enable ingress"
function newminikube {
    minikube delete
    minikube start
    minikube addons enable metrics-server
    minikube addons enable ingress
    pushd /home/oscar/draft/localcert
    kubectl -n kube-system create secret tls mkcert --key key.pem --cert cert.pem
    dasel put string '.KubernetesConfig.CustomIngressCert' 'kube-system/mkcert' -f $HOME/.minikube/profiles/minikube/config.json
    minikube addons disable ingress
    minikube addons enable ingress
    popd
}
# generate terraform readme
alias tfdocs="tf-docs markdown terraform_module/ --output-file ../README.md"
# search for arg in history and open results in less
function lesshist {
    history | grep "$1" | less
}
# search for arg in history and open results in nvim (vi)
# nvim is less performant but allows copying text
function viewhist {
    history | grep "$1" | nvim -c "doautocmd Filetype bash" -
}

# quickly renaming regolith i3 workspaces
# with argument
function rnw {
    __WS_NUMBER=$(i3-msg -t get_workspaces | jq -r 'map(select(.focused))[0].num')
    if [ -z $1 ]
    then
        __WS_NAME="$__WS_NUMBER:<span font_desc='BitstreamVeraSansMono Nerd Font 12'> $__WS_NUMBER </span>"
    else
        __WS_NAME="$__WS_NUMBER:<span font_desc='BitstreamVeraSansMono Nerd Font 12'> $__WS_NUMBER:$1 </span>" # unicode thin space around shown text
        #__WS_NAME="$__WS_NUMBER:<span font_desc='BitstreamVeraSansMono Nerd Font 12'> $__WS_NUMBER:$1 </span>" # unicode hair space around shown text
        #__WS_NAME="$__WS_NUMBER:<span font_desc='BitstreamVeraSansMono Nerd Font 12'>$__WS_NUMBER:$1</span>" # no space around shown text
    fi
    __I3_MSG_COMMAND='rename workspace to "'"$__WS_NAME"'"'
    i3-msg "$__I3_MSG_COMMAND"
}
# with current directory name
function rnwd {
    rnw $(basename $PWD)
}
# with first characters
function rnws {
    rnw $(basename $PWD | cut -c -4)
}

# regenerate completion scripts
# those are not generated at each terminal opening because they take too long (100ms)
function recomplete {
    mkdir -p $HOME/.bash_completion.d
    # Kubectl completion
    kubectl completion bash > ~/.bash_completion.d/kubectl

    # Helm completion
    helm completion bash > ~/.bash_completion.d/helm

    # Minikube completion
    minikube completion bash > ~/.bash_completion.d/minikube

    # Scaleway
    scw autocomplete script shell=bash > ~/.bash_completion.d/scw
}

# I can't remember the EOT syntax in terraform
function tfeot {
    echo "<<-EOT
  something something
EOT" | tee /dev/tty | xclip -sel clip
}
# nor the file syntax in terraform
function tffile {
    echo "file(\"\${path.module}/somefile.yaml\")" | tee /dev/tty | xclip -sel clip
}
# nor the cat EOF syntax in bash
function eof {
    echo "cat << EOF > some.file
something something
EOF" | tee /dev/tty | xclip -sel clip
}
function cateof {
    eof
}
# search for todos
alias todo="grep -riw todo"
# load gitlab token from env var
alias tfgltoken='export TF_VAR_gitlab_token=$GITLAB_TOKEN'
# fetch varfile from gitlab
function dlvarfile {
    GITLAB_URL=$(cat .git/config | grep url | grep gitlab | head -n 1 | sed 's/^\s*url = git@gitlab\.com:\(.*\)\.git$/https:\/\/gitlab.com\/\1/')
    PROJECT_ID=$(curl -sL $GITLAB_URL | grep input | grep hidden | grep project_id | sed 's/.*value="\([0-9]\+\)".*$/\1/')
    VAR_FILE_JSON=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/variables/VAR_FILE")
    SAVE_UMASK=$(umask)
    umask 0077
    echo $VAR_FILE_JSON | jq -r .value > terraform.tfvars.tmp
    mv --backup=t terraform.tfvars.tmp terraform.tfvars
    umask $SAVE_UMASK
}

# stray
function stray {
    export STEAM_COMPAT_DATA_PATH=/home/oscar/proton
    export STEAM_COMPAT_CLIENT_INSTALL_PATH=".local/share/Steam/compatibilitytools.d/GE-Proton7-31/proton"
    mkdir -p $STEAM_COMPAT_DATA_PATH
    alias stray='.local/share/Steam/compatibilitytools.d/GE-Proton7-31/proton run /home/oscar/Downloads/Stray\ v.1.2\ \(2022\)/Stray/Stray.exe'
}

# sound
export AUDIO_SPEAKERS_SINK="alsa_output.usb-Logitech_Logitech_Z-5_Speakers-00.analog-stereo"
export AUDIO_HEADPHONES_SINK="alsa_output.pci-0000_00_1f.3.analog-stereo"
function __get_sink_idx {
    pacmd list-sinks | grep -B 1 "name: <$1>$" | grep 'index: ' | sed 's/.*index: //'
}
function __get_sink_inputs {
    pacmd list-sink-inputs | grep -B 4 -E "sink: [0-9]+ <$1>$" | grep 'index: ' | sed 's/.*index: //'
}
function __move_sink_inputs {
    __src=$1
    __dest=$2
    __idxs=$(__get_sink_inputs $__src)
    for __idx in $__idxs
    do
        pacmd move-sink-input $__idx $(__get_sink_idx $__dest)
    done
}
function toggleaudio {
    __current_sink=$(pacmd list-sinks | grep -E -A 1 '^  \* index: [0-9]+$' | grep 'name: <' | sed 's/^.*name: <//' | sed 's/>$//')
    if [ $__current_sink == $AUDIO_SPEAKERS_SINK ]
    then
        pacmd set-default-sink $AUDIO_HEADPHONES_SINK && __move_sink_inputs $AUDIO_SPEAKERS_SINK $AUDIO_HEADPHONES_SINK
    elif [ $__current_sink == $AUDIO_HEADPHONES_SINK ]
    then
        pacmd set-default-sink $AUDIO_SPEAKERS_SINK && __move_sink_inputs $AUDIO_HEADPHONES_SINK $AUDIO_SPEAKERS_SINK
    else
        echo "The current sink is $__current_sink, there's something wrong with your setup"
    fi
}

# get passwords from kubernetes
# the envs are described in a .gpfk.yaml file
function gpfk {
    __shorthand=$1
    __db="$(cat $HOME/.gpfk.yaml | dasel -r yaml -w json)"
    __search="$(echo "$__db" | jq --arg shorthand "$__shorthand" '.[$shorthand]')"
    if [ "$__search" == "null" ]
    then
        echo "Unknown shorthand"
    else
        __context="$(echo "$__search" | jq -r '.context')"
        __namespace="$(echo "$__search" | jq -r '.namespace')"
        __secret="$(echo "$__search" | jq -r '.secret')"
        __data="$(echo "$__search" | jq -r '.data')"
        __password=$(kubectl --context "$__context" -n "$__namespace" get secret "$__secret" -o json | jq -r --arg data "$__data" '.data | .[$data]' | base64 -d)
        echo $__password
        echo $__password | copy
    fi
}
_gpfk_completions()
{
  COMPREPLY=($(compgen -W "$(cat $HOME/.gpfk.yaml | dasel -r yaml -w json | jq -r 'keys | .[]')" "${COMP_WORDS[1]}"))
}
complete -F _gpfk_completions gpfk

# debug pod
function debugpod {
    cat << EOF
apiVersion: v1
kind: Pod
metadata:
  name: debug-$(date +%s)
spec:
  containers:
  - name: alpine
    image: alpine:latest
    command:
    - sleep
    args:
    - "36000"
  restartPolicy: Never
EOF
}
function glprojecturl {
    PROJECT_DIR=$1
    GITLAB_URL="$(git -C $PROJECT_DIR config --get remote.origin.url | sed 's/^git@gitlab\.com:\(.*\)\.git$/https:\/\/gitlab.com\/\1/')"
    echo $GITLAB_URL
}
function glprojectid {
    PROJECT_DIR=$1
    GITLAB_URL=$(glprojecturl $PROJECT_DIR)
    PROJECT_ID=$(curl -s $GITLAB_URL | grep input | grep hidden | grep project_id | sed 's/.*value="\([0-9]\+\)".*$/\1/')
    echo $PROJECT_ID
}

# find latest terraform module version
function tfmodver {
    MODULE_NAME=$1
    MODULES_GROUP='/home/oscar/glab/vigigloo/tf-modules'
    TOOLS_GROUP='/home/oscar/glab/vigigloo/tools'
    if [ -d "$MODULES_GROUP/$MODULE_NAME" ]
    then
        MODULE_PATH="$MODULES_GROUP/$MODULE_NAME/terraform_module" 
    elif [ -d "$TOOLS_GROUP/$MODULE_NAME" ]
    then
        MODULE_PATH="$TOOLS_GROUP/$MODULE_NAME/terraform_module" 
    else
        echo "No valid module found"
        return
    fi
    GITLAB_URL="$(cat $(dirname $MODULE_PATH)/.git/config | grep url | grep gitlab | sed 's/^\s*url = git@gitlab\.com:\(.*\)\.git$/https:\/\/gitlab.com\/\1/')"
    PROJECT_ID=$(curl -s $GITLAB_URL | grep input | grep hidden | grep project_id | sed 's/.*value="\([0-9]\+\)".*$/\1/')

    TF_PACKAGES="$(curl -s "https://gitlab.com/api/v4/projects/$PROJECT_ID/packages?page=1&per_page=20&sort=desc&order_by=created_at&package_type=terraform_module")"
    echo "$TF_PACKAGES" | jq -r '.[] | "source = \"gitlab.com/vigigloo/" + .name + "\"\nversion = \"" + .version + "\""'
}
_tfmodver_completions()
{
    MODULES_GROUP='/home/oscar/glab/vigigloo/tf-modules'
    TOOLS_GROUP='/home/oscar/glab/vigigloo/tools'
    COMPREPLY=($(compgen -W "$( (find $MODULES_GROUP -name .git -type d ; find $TOOLS_GROUP -name .git -type d) | xargs -n 1 dirname | xargs -n 1 basename)" "${COMP_WORDS[1]}"))
}
complete -F _tfmodver_completions tfmodver

# run all checks for terraform
function tffullcheck {
    echo terraform init
    terraform init
    echo terraform validate
    terraform validate
    echo terraform fmt -recursive
    terraform fmt -recursive
    echo "docker run --rm -v $(pwd):/data ghcr.io/terraform-linters/tflint-bundle:v0.42.2.0"
    docker run --rm -v $(pwd):/data ghcr.io/terraform-linters/tflint-bundle:v0.42.2.0
}
function tfinit {
    PROJECT_DIR=$1
    if [ -z "$PROJECT_DIR" ]
    then
        PROJECT_DIR=$(pwd)
    fi
    PROJECT_ID=$(glprojectid $PROJECT_DIR)
    PROJECT_NAME=$(basename "$PROJECT_DIR")
    cat << EOF > $PROJECT_DIR/init
terraform init \\
    -backend-config="address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$PROJECT_NAME" \\
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$PROJECT_NAME/lock" \\
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/$PROJECT_NAME/lock" \\
    -backend-config="username=ohemelaar" \\
    -backend-config="password=\$GITLAB_TOKEN" \\
    -backend-config="lock_method=POST" \\
    -backend-config="unlock_method=DELETE" \\
    -backend-config="retry_wait_min=5"
EOF
}

# export dotfiles to a tmp dir keeping the folder structure
function dotfiles {
    tmpdir=/tmp/dotfiles-$(date -I)
    mkdir $tmpdir
    for file in {\
README.md,\
.bashrc,.bash_aliases,.bash_completion,.profile,\
.git/,.gitconfig,.gitignore,\
.inputrc,\
.taskrc,\
.vim/{autoload/,plugged/},.vimrc,\
.config/VSCodium/User/settings.json,.vscode-oss/extensions/extensions.json,\
.config/regolith2/{Xresources,i3/config.d/},default-neutral/,\
.config/{gitui/key_bindings.ron,nvim/}\
}
    do
        mkdir -p "$tmpdir/$(dirname $file)"
        cp -r $HOME/$file $tmpdir/$file
    done
    echo "dotfiles available at $tmpdir"
}
function gl {
    xdg-open $(glprojecturl .) 2>/dev/null
}
