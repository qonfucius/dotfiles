# Oscar Dotfiles

- [bash](https://www.gnu.org/software/bash/) because it's widely adopted, supported and installed by default
- [regolith-desktop](https://regolith-desktop.com/) desktop environment based on i3
- [vscodium](https://vscodium.com/) as GUI editor
- [neovim](https://neovim.io/) as terminal editor
- [vscode-neovim](https://open-vsx.org/extension/asvetliakov/vscode-neovim) to get the best of both worlds
- [gitui](https://github.com/extrawurst/gitui) because committing, pushing, staging... takes too much time without it
- [taskwarrior](https://taskwarrior.org/) to free up brain space
