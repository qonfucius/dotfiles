# Contribution workflow

This repository is mainly dedicated to our team but if you have a need or an improvement it is open to external contribution. Please use a Pull Request on a specific branch with an explicit name (e.g `feat/add-lsp-support-neovim`).

For our team, each of us mainly contributes to its own folder thanks to Pull Requests, we can prefix branches with our firstname. For instance it could be: `charles/add-i3-config`.

If we want to contribute to an other folder, please make a Pull Request and add the folder owner to the reviewers.

Those three points should be enough to have an easy to use repository, thanks for your contributions!
