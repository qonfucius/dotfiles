if [[ ! $ZCONFIGDIR ]]; then
    ZCONFIGDIR="$PWD"
    export ZCONFIGDIR
fi

PARTIAL_CONFIGS_DIR="${ZCONFIGDIR}/conf.d"
for conf in $PARTIAL_CONFIGS_DIR/*.zsh; do
    source "$conf"
done

