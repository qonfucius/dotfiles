if type "exa" > /dev/null; then
    alias ls='exa --icons -1'
    alias lg='ls -G'
    alias lll='ls -hl --time-style iso'
    alias llt='lll -T -L 1'
    alias ll='lll --no-permissions --no-user'
    alias lt='ll -T -L 1'
fi
