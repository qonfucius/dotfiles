ZSH_PLUGINS_DIR="${ZCONFIGDIR}/plugins"

# ZSH additionnal completions https://github.com/zsh-users/zsh-completions
fpath=($ZSH_PLUGINS_DIR/zsh-completions/src $fpath)

# Auto-Suggestions https://github.com/zsh-users/zsh-autosuggestions
source $ZSH_PLUGINS_DIR/zsh-autosuggestions/zsh-autosuggestions.zsh

# Syntax highlighting https://github.com/zdharma-continuum/fast-syntax-highlighting
source $ZSH_PLUGINS_DIR/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh

# Substring Search https://github.com/zsh-users/zsh-history-substring-search
source $ZSH_PLUGINS_DIR/zsh-history-substring-search/zsh-history-substring-search.zsh
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down

# Spaceshipt Prompt https://github.com/spaceship-prompt/spaceship-prompt
source $ZSH_PLUGINS_DIR/spaceship-prompt/spaceship.zsh
