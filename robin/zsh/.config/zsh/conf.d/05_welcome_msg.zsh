if type "neofetch" > /dev/null; then
    echo "\n"
    neofetch --ascii_distro Tux --colors 4 1 5 2 1 3 --color_blocks off
fi

