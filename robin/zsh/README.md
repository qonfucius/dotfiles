# Robin ZSH config

```sh
 zsh/
├──  .config/
│  └──  zsh/ # zsh directory (~/.config/zsh)
│     ├──  .zshrc # .zshrc loaded by root ~/.zshrc
│     ├──  conf.d/ # Autoloaded (priority by name) confs files loaded by ../.zshrc
│     │  ├──  05_welcome_msg.zsh
│     │  ├──  10_ls.zsh
│     │  ├──  15_aliases.zsh
│     │  ├──  15_binds.zsh
│     │  ├──  15_options.zsh
│     │  ├──  20_history.zsh
│     │  ├──  20_plugins.zsh # Plugins declaration
│     │  ├──  25_completion.zsh
│     │  └──  25_prompt.zsh
│     └──  plugins/ # Plugins intall directory (git clone)
#        ├──  fast-syntax-highlighting
#               https://github.com/zdharma-continuum/fast-syntax-highlighting
#        ├──  spaceship-prompt
#               https://github.com/spaceship-prompt/spaceship-prompt
#        ├──  zsh-autosuggestions
#               https://github.com/zsh-users/zsh-autosuggestions
#        ├──  zsh-completions
#               https://github.com/zsh-users/zsh-completions
#        └──  zsh-history-substring-search
#               https://github.com/zsh-users/zsh-history-substring-search
├──  .zshrc # root .zshrc (~/.zshrc)
```