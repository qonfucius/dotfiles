# Robin Tilix conf

Tilix configuration exported from dconf

```sh
dconf dump /com/gexperts/Terminix/ > tilix.dconf # export
dconf load /com/gexperts/Terminix/ < tilix.dconf # import
```
