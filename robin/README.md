# Robin Dotfiles

- [zsh](https://gitlab.com/qonfucius/dotfiles/-/tree/main/robin/zsh)
- [tilix](https://gitlab.com/qonfucius/dotfiles/-/tree/main/robin/tilix)
- [gnome-shell](https://gitlab.com/qonfucius/dotfiles/-/tree/main/robin/gnome-shell)
- [neovim](https://gitlab.com/qonfucius/dotfiles/-/tree/main/robin/neovim)
